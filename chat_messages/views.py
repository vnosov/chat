from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from chat_messages.models import Message
from chat_messages.serializers import MessageCreateSerializer, MessageSerializer


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageCreateSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.action in ('list',):
            return MessageSerializer
        else:
            return MessageCreateSerializer

    def get_queryset(self):
        return Message.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
