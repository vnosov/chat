from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from chat_messages.models import Message
from groups.serializers import GroupSerializer
from users.serializers import UserSerializer


class MessageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'text', 'group')

    def validate_group(self, group):
        try:
            user = group.users.get(id=self.context['request'].user.id)
            if user:
                return group
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                'You can\'t write messages in this group. You don\'t belong to this group.'
            )


class MessageSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    user = UserSerializer()

    class Meta:
        model = Message
        fields = ('id', 'text', 'creation_date', 'user', 'group')
