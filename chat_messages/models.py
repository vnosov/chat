from django.db import models
from django.utils import timezone

from groups.models import Group
from users.models import User


class Message(models.Model):
    text = models.CharField(max_length=500)
    creation_date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        db_table = 'message'

    def __str__(self):
        return self.text
