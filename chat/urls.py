"""chat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))


    REST API для чата.
    +1. Пользователи могут создавать группы.
    +2. Пользователи могут читать и добавлять сообщения в группу.
    +3. Администратор может изменять название группы и описание.
    +4. Если кто-то в группе, то он может добавить в нее другого участника.
    +5. Удалить кого-то из группы может только администратор.
    +6. Авторизация по токену.

    + по view - используй mixins и viewset
    + по urls - используй роутеры и HTTP verb (один URL должен уметь принимать разные типы запросов)
    + user serializer - по идее, там перед созданием пользователя нужно удалить password из validated data
    + кастомный пользователь - подозрительно много кода. В документации сказано только про две строчки изменений.
    + Использование all в сериалайзере - не надо
    + странный (видимо не доделанный) serializer для messages group

    В целом получше, но есть пара замечаний:
    + Неиспользуемый import (users models).
    + Посмотри что такое router и как его использовать с ViewSet.
    + У групп должны быть локальные админы, которые только этой группой управляют.
    Добавление/удаление участников из группы - не по rest:
    + для добавления участника надо бы отправлять POST запрос с ид добавляемого (или добавляемых) на url 'api/groups/{group_id}/participants';
    + для удаления надо бы отправлять DELETE запрос с ид добавляемого(или  добавляемых) на url 'api/groups/{group_id}/participants/{participant_id}';
    можно еще сделать отправку PUT админом группы на 'api/groups/{group_id}/participants'.
    + Проверка на принадлежность участника/админа группе - видел разные реализации, но я бы вынес в отдельный permission все же.


    Чуть не забыл, прежде чем запаковать в docker,
    настрой nginx на перенаправление запросов на Django.
    В результате при запуске контейнеров должна запускаться Production ready система)
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from groups import views as groups_view
from users import views as users_view
from chat_messages import views as messages_view

router = DefaultRouter()
router.register(r'users', users_view.UserViewSet)
router.register(r'groups', groups_view.GroupViewSet)
router.register(r'messages', messages_view.MessageViewSet, base_name='messages')


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
]
