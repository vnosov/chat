from rest_framework import serializers

from groups.models import Group, GroupUser


class GroupUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = GroupUser
        fields = ('participant', 'is_admin')


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'description')

    def create(self, validated_data):
        group = Group.objects.create(
            name=validated_data['name'],
            description=validated_data['description'],
        )
        group_user = GroupUser(
            group_id=group.id,
            participant_id=self.context['request'].user.id
        )
        group_user.save()
        group.groupuser_set.add(group_user)
        return group


class GroupEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'description', 'users')


class GroupAddUserSerializer(serializers.ModelSerializer):
    users = GroupUserSerializer()

    class Meta:
        model = GroupUser
        fields = ('users',)
