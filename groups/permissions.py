from rest_framework.permissions import BasePermission


class UserInGroup(BasePermission):
    SAFE_METHODS = ['POST', 'GET']

    def has_permission(self, request, view):
        return request.user.is_authenticated and request.method in self.SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        user_in_group = any(user == request.user for user in obj.users.all())
        is_admin_of_group = \
            any(group_user.participant == request.user and group_user.is_admin for group_user in obj.groupuser_set.all())
        return user_in_group and not is_admin_of_group and request.method in self.SAFE_METHODS


class IsAdminOfGroup(BasePermission):
    SAFE_METHODS = ['PUT', 'GET']

    def has_object_permission(self, request, view, obj):
        is_admin_of_group = \
            any(group_user.participant == request.user and group_user.is_admin for group_user in obj.groupuser_set.all())
        return is_admin_of_group and request.method in self.SAFE_METHODS
