from django.db import models

from users.models import User


class Group(models.Model):
    name = models.CharField(max_length=30, unique=True)
    description = models.CharField(max_length=250, null=True)
    users = models.ManyToManyField(User, through='GroupUser')

    class Meta:
        db_table = 'group'

    def __str__(self):
        return self.name


class GroupUser(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    participant = models.ForeignKey(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)

    class Meta:
        db_table = 'group_user'
        unique_together = ('group', 'participant')
