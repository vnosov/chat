from django.db import IntegrityError
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from groups.models import Group, GroupUser
from groups.permissions import UserInGroup, IsAdminOfGroup
from groups.serializers import GroupSerializer, GroupAddUserSerializer, GroupEditSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    @action(detail=True, url_path='participants', methods=['POST', 'PUT'])
    def add_participants(self, request, *args, **kwargs):
        group = self.get_object()
        try:
            for user_id in request.data.getlist('users.participant'):
                is_admin = True if request.data.get('users.is_admin') else False
                group_user = GroupUser(
                    group_id=group.id,
                    participant_id=user_id,
                    is_admin=is_admin
                )
                group_user.save()
                group.groupuser_set.add(group_user)
        except IntegrityError:
            pass
        return Response({'users': request.data.getlist('users.participant')})

    @action(detail=True, url_path='participants/(?P<participant_id>[a-z0-9]+)', methods=['DELETE'])
    def delete_participant(self, request, *args, **kwargs):
        group = self.get_object()
        group.users.remove(kwargs['participant_id'])
        return Response({'status': 'OK'})

    def get_permissions(self):
        if self.action in ('update', 'delete_participant'):
            permission_classes = [IsAdminUser, IsAdminOfGroup]
        elif self.action in ('add_participants',):
            if self.request.method == 'PUT':
                permission_classes = [IsAdminOfGroup]
            else:
                permission_classes = [UserInGroup]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action in ('partial_update', 'update'):
            if self.request.user.is_staff:
                return GroupEditSerializer
        elif self.action in ('add_participants',):
            return GroupAddUserSerializer
        else:
            return GroupSerializer

    def perform_create(self, serializer):
        serializer.save(users=[self.request.user])
